import logo from './logo.svg';
import './App.css';
import { useState } from 'react'


function App() {
  //wypluwa zmienna i funkcje do jej zmiany (dynamicznej)
  const [count, setCount] = useState(0);
  //funkcja do zmiany
  function handleClick() {
    setCount(count + 1);
  }
  return (
    <section>
      <h1>Test</h1>
      <MyButton count={count} onclick={handleClick}></MyButton>
      <MyButton count={count} onclick={handleClick}></MyButton>
      <MyButton count={count} onclick={handleClick}></MyButton>
      <MyButton count={count} onclick={handleClick}></MyButton>
      <br></br>
      <MyButtonek></MyButtonek>
      <MyButtonek></MyButtonek>
    </section>
  );
}
function MyButton({ count, onclick }) {
  return (
    <button onClick={onclick}>
      {count}
    </button>
  )
}
function MyButtonek() {
  const [counter, setCounter] = useState(0);
  function handleClick() {
    setCounter(counter + 1);
  }
  return (
    <button onClick={handleClick}>
      To inny przycisk {counter}
    </button>
  )
}
export default App;
